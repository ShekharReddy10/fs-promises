const path = require("path");
const fsp = require("fs/promises");

function manipulateFile(fileName) {
  fsp
    .readFile(fileName, "utf-8")
    .then((data) => {
      console.log("Read the given file.");
      return fsp.writeFile("upperCase.txt", data.toUpperCase());
    })
    .then(() => {
      console.log("Converted text in to uppercase.");
      return fsp.writeFile("filename.txt", "upperCase.txt\n");
    })
    .then(() => {
      console.log("Stored upperCase.txt file name into filename.txt");
      return fsp.readFile("upperCase.txt", "utf-8");
    })
    .then((data) => {
      console.log("Read data from new file upperCase.txt");
      let toLowerCase = data.toLowerCase();
      let sentences = toLowerCase
        .replace(/(\r\n|\n|\r)/g, "")
        .split(". ")
        .join(".\n");
      return fsp.writeFile("lowerCase.txt", sentences);
    })
    .then(() => {
      console.log(
        "Converted into loweCase and sentences and written into lowerCase.txt "
      );
      return fsp.appendFile("filename.txt", "lowerCase.txt\n");
    })
    .then(() => {
      console.log("Stored lowerCase.txt file name into filename.txt ");
      return fsp.readFile("lowerCase.txt", "utf-8");
    })
    .then((data) => {
      console.log("read data from lowerCase.txt");
      let joinSentence = data.toString().split("\n");
      let sortedData = joinSentence.sort().slice(1);
      fsp.writeFile("sortedData.txt", sortedData);
    })
    .then(() => {
      console.log("Sorted the data and written sortedData.txt ");
      return fsp.appendFile("filename.txt", "sortedData.txt");
    })
    .then(() => {
      console.log("Stored sortedData.txt file name into filename.txt ");
      return fsp.readFile("filename.txt", "utf-8");
    })
    .then((files) => {
      console.log("Read file names from the filenmae.txt");
      let filesArray = files.split("\n");
      filesArray.forEach((file) => {
        return fsp.unlink(file);
      });
    })
    .then(() => {
      console.log("Files are deleted");
    })
    .catch((err) => {
        console.error(err);
    })
}
module.exports =  manipulateFile;
