const fsp = require("fs/promises");
const path = require("path");
const directory = path.join(__dirname, "./Random");

function createFiles(directory, fileName) {
  fsp.writeFile(
    path.resolve(directory, fileName),
    JSON.stringify([1, 2, 3]),
    "utf-8"
  );
  console.log(`${fileName} added to Random directory`);
}
function createAndDeleteFiles(maxFilesRandomnlyGenerated){
fsp
  .mkdir(path.join(directory), { recursive: true })
  .then(() => {
    let noOfFiles = Math.floor(Math.random() * maxFilesRandomnlyGenerated);
    console.log(`Number of Random files generated = ${noOfFiles}`);
    for (let countFile = 1; countFile <= noOfFiles; countFile++) {
      let fileName = `file${countFile}.json`;
      if (directory !== undefined && fileName !== undefined) {
        Promise.resolve(createFiles(directory, fileName));
      } else {
        Promise.reject(console.error(err));
      }
    }
  })
  .then(() => {
    console.log("Files are created");
      return fsp.readdir(directory, "utf-8");
    })
    .then((files) => {
      console.log("Read files from the directory");
      files.forEach((file) => {
        return fsp.unlink(path.join(directory,file));
      });
    })
  .catch((err) => {
    console.error(err);
  });
}
//createAndDeleteFiles();
module.exports = createAndDeleteFiles;